#include <iostream>
#include <array>
#include <vector>
#include <set>
#include <memory>
#include <string>
#include "consts.h"
#include "Picks_And_Reservation_Matrix.h"
#include "Max_Bid_Computer.h"
#include "Test_Cases.h"
#include <homebrew/test/tools.h>
#include <chrono> 
#include "homebrew/ThreadPool/Runnable.h"
#include <thread>
#include "VCG_Auction.h"
int bid_nr = 8;
static_assert(std::is_integral<int_t>::value, "parameter type is not integral");
static_assert(sizeof(int_t)*8 >= cMax_item_nr, "too small int size, change int_t just above to smt larger");
std::vector<int> buyer_ids(bid_nr);
std::array<int_t, cMax_item_nr> masks { initialize_masks() };
Picks_And_Reservation_Matrix mat(bid_nr);
Test_Case1 test1{};
VCG_Auction auction_obj{ test1.test_input1.size1(), 3, test1.test_input1 };
Tests tests{};
Test3 test3{};
using iterator1 = Picks_And_Reservation_Matrix::iterator1;

int main() {
	test1.test_input1.print(3);
	auto vec = mat.create_submatrices();
	auction_obj.set_jobs();
	auction_obj.start();
	auto stop = chrono::steady_clock::now();
	std::cout << std::endl;
	if constexpr(Test::ctest) system("pause");
};

