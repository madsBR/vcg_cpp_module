#pragma once
#include <homebrew/test/tools.h>
#include "consts.h"
#include "Picks_And_Reservation_Matrix.h"
#include "Max_Bid_Computer.h"
#include <random>
#include "homebrew/ThreadPool/Conc_Jobs_Container.h"
#include "homebrew/ThreadPool/ThreadPool.h"
#include "homebrew/ThreadPool/Runnable.h"

static_assert(Test::ctest, "Do not include test_cases in release version. This is just meant for testing");



struct Test_Case1
{
public:
	constexpr static int buyers_nr{ 3 };
	constexpr static int items{ 3 };
	constexpr static int bid_nr{items*buyers_nr * 2};
	constexpr static int bid_per_buyer = bid_nr / buyers_nr;
	int picks[bid_nr]{
		0b100,
		0b100,
		0b010,
		0b010,
		0b001,
		0b001
	};
	int reservations[bid_nr]{
		0b100,
		0b110,
		0b010,
		0b011,
		0b001,
		0b101
	};
	int bid_values[bid_nr] = {
		3,8,4,9,0,8,
		5,9,2,8,4,11,
		4,8,2,9,4,12

	};
	Picks_And_Reservation_Matrix test_input1{ bid_nr };
	Test_Case1() {
		for (int i = 0; i < bid_nr; i++) {
			test_input1(i, Picks_And_Reservation_Matrix::eMat_cols::eBids) = bid_values[i];
			test_input1(i, Picks_And_Reservation_Matrix::eMat_cols::ePicks) = picks[i % bid_per_buyer];
			test_input1(i, Picks_And_Reservation_Matrix::eMat_cols::eReservations) = reservations[i % bid_per_buyer];
			test_input1(i, Picks_And_Reservation_Matrix::eMat_cols::eBuyers) = i / bid_per_buyer;
		}
	};

	bool run() {
		bool result = true;
		Max_Bid_Computer max_bid_obj(buyers_nr, test_input1);
		max_bid_obj.start();
		if (max_bid_obj.get_max() != 16) {
			std::cout << "failed, correct max should be " << 16;
			result = false;
		}
		if (*max_bid_obj.winning_bids[0] != 4 || *max_bid_obj.winning_bids[1] != 12 ) {
			std::cout << "failed, correct winning bids should be " << 4 << ", " << 12;
			result = false;
		}
		return result;
	}
};

struct Test_Case2
{
	using vector = std::vector<int>;
public:
	std::mt19937 e;
	const int buyers_nr;
	const int bid_per_buyer;
	const int item_per_bid;
	const int reservations_per_bid;
	const int item_nr;
	const int bid_nr;

	Picks_And_Reservation_Matrix test_input2{ bid_nr };
	Test_Case2(int buyers_nr_, int bid_per_buyer_, int item_per_bid_, int reservations_per_bid_,int item_nr = 8) : buyers_nr{ buyers_nr_ }, bid_per_buyer{ bid_per_buyer_ }, item_per_bid{ item_per_bid_ }, reservations_per_bid{ reservations_per_bid_ }, item_nr{ 8 }, bid_nr{ bid_per_buyer * buyers_nr }, e{ 42 } {
		for (int i = 0; i < bid_nr; i++) {
			
			test_input2(i, Picks_And_Reservation_Matrix::eMat_cols::eBids) = rd_int(12);
			int sum = 0;
			for (int i = 0; i < item_per_bid; i++)	sum = sum | encode_item(rd_int(item_nr));
			test_input2(i, Picks_And_Reservation_Matrix::eMat_cols::ePicks) = sum ;
			for (int i = 0; i < reservations_per_bid; i++)	sum = sum | encode_item(rd_int(item_nr));
			test_input2(i, Picks_And_Reservation_Matrix::eMat_cols::eReservations) = sum;
			test_input2(i, Picks_And_Reservation_Matrix::eMat_cols::eBuyers) = i / bid_per_buyer;
		}
	};
	inline int rd_int(int limit = 32) { return e() % limit; }
	inline int encode_item(int item_id) {
		return 1 << item_id;
	}
	bool run() {
		bool result{ true };
		Max_Bid_Computer max_bid_obj(4, test_input2);
		max_bid_obj.start();
		if (max_bid_obj.get_max() != 28) {
			std::cout << "failed, correct max should be " << 28 << std::endl;
			result = false;
		}
		if (*max_bid_obj.winning_bids[0] != 6 || *max_bid_obj.winning_bids[1] != 11 || *max_bid_obj.winning_bids[2] != 11) {
			std::cout << "failed, correct winning bids should be " << 6 << ", " << 11 << ", " << 11 << *max_bid_obj.winning_bids[0];
			result = false;
		}
		if (result) std::cout << "test 2 passed" << "\n";
		return result;
	}

};



struct Test3_Runnable1 : public Runnable {
	bool result;
	Test_Case1 test_obj1{};
	virtual void job_to_run() override {result = test_obj1.run();}
	virtual bool get_result() { return result; };
};

struct Test3_Runnable2 : public Runnable {
	bool result;
	Test_Case2 test_obj2{ 4,2,2,1,8 };
	virtual void job_to_run() override { result = test_obj2.run(); }
	virtual bool get_result() { return result; };

};

class Waiter : public Runnable {
	virtual void job_to_run() override {
		std::cout << "BEGINNING TO SLEEP";
		this_thread::sleep_for(10s);
		std::cout << "WOKE UP";
	}
};
class Test3 {
	Conc_Jobs_Container container{};
	Test3_Runnable1 test1{};
	Test3_Runnable2 test2{};
	Test3_Runnable1 test3{};
	Test3_Runnable2 test4{};
	Waiter waiter{};
	ThreadPool pool{ container, 2 };

public:
	Test3(){
		container.add_runnable(test1,false);
		container.add_runnable(test2, false);
		container.add_runnable(test3, false);
		container.add_runnable(test4, false);
		container.add_runnable(waiter, false);
		container.add_latch(2);

	}

	bool run() {
		std::cout << "Starting test 3, concurrency test of case 1 and 2";
		pool.start();
		if (test1.result && test2.result && test3.result && test4.result) {
			std::cout << "Test 3 passed";
			return true;
		}
		else {
			std::cout << "Test 3 failed";
			return false;
		}
	}
};

struct Tests {
	Test_Case1 test_obj1{};//basic test
	Test_Case2 test_obj2{ 4,2,2,1,8 };

	bool run() {
		bool result = true;
		result = result && test_obj1.run();
		result = result && test_obj2.run();
		return result;

	}

};

