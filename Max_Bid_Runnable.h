#pragma once
#include "homebrew/ThreadPool/Runnable.h"
#include "Max_Bid_Computer.h"
#include "Picks_And_Reservation_Matrix.h"
#include <algorithm>
#include <atomic>


//TODO: Collect this into one with SFINAE.
template <typename out_iter,typename out_iter_winner>
struct Max_Bid_Runnable_W_Winners: public Runnable
{
	out_iter max_out;
	out_iter_winner winners_out;
	int from;
	int to;
	Max_Bid_Computer max_bid_computation;
		Max_Bid_Runnable_W_Winners(int buyers_nr, Picks_And_Reservation_Matrix& mat,out_iter max_out_,out_iter_winner winners_out_, int from = 0, int to = 0)
			: max_bid_computation(buyers_nr, mat), max_out{ max_out_ }, winners_out{ winners_out_ }, from{ from }, to{ to }{}

		virtual void job_to_run() override{
		max_bid_computation.start(from,to);
		*max_out = max_bid_computation.get_max(); 
	}
};

template <typename out_iter>
struct Max_Bid_Runnable : public Runnable
{
	out_iter max_out;
	int from;
	int to;
	Max_Bid_Computer max_bid_computation;
	Max_Bid_Runnable(int buyers_nr, Picks_And_Reservation_Matrix& mat, out_iter max_out_,  int from = 0, int to = 0)
		: max_bid_computation(buyers_nr, mat), max_out{ max_out_ },  from{ from }, to{ to }{}

	virtual void job_to_run() override {
		max_bid_computation.start(from, to);
		*max_out = max_bid_computation.get_max();
	}
};


