#pragma once
#include "Picks_And_Reservation_Matrix.h"
#include <homebrew/Array2D/Array2D.h>
#include <algorithm>
#include <numeric>
#include "homebrew/test/tools.h"
class Max_Bid_Computer
{
	
	using Bid_Id = Picks_And_Reservation_Matrix::iterator1;	
	using Pred = Picks_And_Reservation_Matrix::Is_Bids_Conform_Pred;
	using iter_conf_col = Array2D<Bid_Id>::iterator2;

	bool completed = false;
	int max = 0;
	int buyers_nr;
	Picks_And_Reservation_Matrix mat;
	Array2D<Bid_Id> conform_mat;
	std::vector<Pred> preds;
	std::vector<iter_conf_col> current_endings;
	std::vector<Bid_Id> current_bids;

	
	void recurser(Bid_Id bid, int level) {
		if constexpr (Test::ctest_verbosity) std::cout << "CALLLED WITH " << "BID " << bid - mat.begin1() << " AND LEVEL " << level;
		bool eval{ false };
		int next_level = level + 1;
		current_bids[level] = bid;
		if constexpr (Test::ctest_verbosity) {
			std::cout << "CONFORM MAT IDS " << std::endl;
			for (int i = 0; i < next_level; i++) {
				for (auto iter2 = conform_mat.begin2(i); iter2 < current_endings[i]; iter2++) {
					std::cout << *iter2 - mat.begin1() << " ";
				}
				std::cout << std::endl;
			}
		}
		if constexpr (Test::ctest_verbosity) {
			std::cout << "CURR BIDS " << std::endl;
			for (int i = 0; i < next_level; i++) std::cout << current_bids[i] - mat.begin1() << " ";
		}
		if (next_level < buyers_nr) {
			iter_conf_col iter2 = conform_mat.begin2(next_level);
			current_endings[next_level] =
				std::copy_if<iter_conf_col, iter_conf_col, Pred>(
					conform_mat.begin2(level),
					current_endings[level],
					conform_mat.begin2(next_level),
					preds[bid - mat.begin1()]);
			if (current_endings[next_level] > conform_mat.begin2(next_level)) {

				for (auto iter = conform_mat.begin2(next_level); iter < current_endings[next_level]; iter++) {
					recurser(*iter, next_level);
				}
			}
			else {
				eval = true;
			};
		}
		else { eval = true; }
		if (eval) {

			int sum = 0;
			for (int i = 0; i < next_level; i++) {
				sum += *(mat.begin2(current_bids[i]) + Picks_And_Reservation_Matrix::eBids);
			}
			if (sum > max) {
				max = sum;
				winner_nr = next_level;
				std::copy(current_bids.begin(),
					current_bids.end(),
					winning_bids.begin());
			}
		}
	};

public:
	int winner_nr;
	std::vector<Bid_Id> winning_bids;
	Max_Bid_Computer(int buyers_nr_, Picks_And_Reservation_Matrix& mat_) :
		buyers_nr{ buyers_nr_ },
		mat{ mat_ },
		conform_mat(buyers_nr_, mat_.size1(), mat_.begin1()),
		preds{},
		current_endings(buyers_nr + 1, conform_mat.end2(0)),
		current_bids(buyers_nr, mat.begin1()),
		winning_bids(buyers_nr, mat.begin1())
	{
		preds.reserve(mat.size1());
		for (auto iter = mat.begin1(); iter != mat.end1(); iter++) preds.emplace_back(mat, iter);
		std::iota(conform_mat.begin2(0), conform_mat.end2(0), mat.begin1());
	}

	void start() { start(0, mat.size2()); }

	void start(int from, int to) {
		for (auto iter = conform_mat.begin2(0) + from; iter != conform_mat.begin2(0) + to; iter++) recurser(*iter, 0);
		completed = true;
		if constexpr (Test::ctest_verbosity) std::cout << "highest value is " << max<<std::endl;
		if constexpr (Test::ctest_verbosity) std::cout << winner_nr << " winners. " << "winning bids:";
		if constexpr (Test::ctest_verbosity) {
			for (auto iter = winning_bids.begin(); iter != winning_bids.end(); iter++) {
				std::cout << *iter - mat.begin1() << " ";
			}
			std::cout << std::endl;
		}
		conform_mat.clear();



	}
	
	bool is_completed() { return completed;	}
	int get_max() { return max; }
	Array2D<int_t>* get_result();
};

