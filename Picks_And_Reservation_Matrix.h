#pragma once
#include <iostream>
#include <array>
#include <vector>
#include <stdint.h>
#include "consts.h"
#include <random>
#include <climits>
#include <algorithm>
#include <homebrew/Array2d/array2D.h> 
class Picks_And_Reservation_Matrix :public Array2D<int_t>
{
public:
	enum eMat_cols : int
	{
		eBids,
		ePicks,
		eReservations,
		eBuyers,
	};
	Picks_And_Reservation_Matrix(int bid_nr);

	void set_bids(); //int* begin_picks, int* end_picks, int* begin_res , int* end_res, int* begin_buyer, int* end_buyer;
	int_t inline get_ele_from_row(iterator1 row, int col) { return *(row.iter_vec + col); }
	int_t inline get_ele_from_row(int row, int col) { return (*this)(row,col); }
//	void inline set_ele_from_row(iterator1 row, int col, int value) {
//	*(row.iter_vec+col ) = }

	std::vector<Picks_And_Reservation_Matrix> create_submatrices(){
		std::vector<Picks_And_Reservation_Matrix> result{};
		iterator1 bid_begin = this->begin1();
		bool found_next_buyer{ false };
		int_t current_buyer{ 0 };

		iterator1 bid_end = this->begin1();
		result.reserve(this->operator()(bid_begin, eBuyers));
		while(bid_end != end1()){
			int_t current_buyer = this->operator()(bid_begin, eBuyers);
			//find_if kinda thing
			found_next_buyer = false;
			while ( (!found_next_buyer) )
			{
				
				if (++bid_end == end1()) found_next_buyer = true;
				else found_next_buyer = this->operator()(bid_end, eBuyers) > current_buyer;
			}
			result.emplace_back(this->size1() - (bid_end - bid_begin));
			std::copy(this->begin(), bid_begin.iter_vec, result.back().begin());
			std::copy(bid_end.iter_vec, this->end(), result.back().begin() + (bid_begin.iter_vec - this->begin()));
			bid_begin = bid_end;
		}
		return result;
	}


	 inline bool is_bids_conform(iterator1 bid1, iterator1 bid2){

			int_t buy_same = ((*this)(bid1, eBuyers) == (*this)(bid2, eBuyers)) ? 1 : 0; //different buyers
			int_t conflict1 = (*this)(bid1, ePicks) & (*this)(bid2, eReservations); //bid1 pick different from bid2 res
			int_t conflict2 = (*this)(bid1, eReservations) & (*this)(bid2, ePicks); //bid2 pick different from bid1 res
			return ! static_cast<bool>(buy_same | conflict1 | conflict2);
	}
	void print(int item_nr = cMax_item_nr) {
		for (int row = 0; row < size1_; row++) {
				std::cout << (*this)(row, eBids) << " ";
				printBin((*this)(row, ePicks),item_nr);
				std::cout << " ";
				printBin((*this)(row, eReservations),item_nr);
				std::cout << " ";
				std::cout << (*this)(row, eBuyers) << " "<<std::endl;
			}
			std::cout << "." << std::endl;
	}

	void printBin(int_t t,size_t size = 0) {
		if (size == 0) size = 8 * sizeof(int);
		for (size_t bit = 0; bit < size; bit++) {
			std::cout << (t & 1);
			t = t >> 1;
		}
	}


	class Is_Bids_Conform_Pred {
		iterator1 bid;
	public:
		Picks_And_Reservation_Matrix& mat;
		Is_Bids_Conform_Pred(Picks_And_Reservation_Matrix& mat, iterator1 bid_) : mat{ mat }, bid { bid_ } {};
		inline bool operator()(iterator1 other_bid) { 
			return mat.is_bids_conform(bid, other_bid); };
	};

};


