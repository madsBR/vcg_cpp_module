#include "Picks_And_Reservation_Matrix.h"
Picks_And_Reservation_Matrix::Picks_And_Reservation_Matrix(int bid_nr) : Array2D<int_t>(bid_nr,4){}

void Picks_And_Reservation_Matrix::set_bids() {
	constexpr uint_fast64_t max_limit = 1 << cMax_item_nr - 1;
	std::default_random_engine generator(314);
	std::uniform_int_distribution<int_t> distribution(0, max_limit );
	std::uniform_int_distribution<int_t> disb_buyers(0, 4);
	for (int i = 0; i < this->size1(); i++) {
		(*this)(i,ePicks) = distribution(generator);
		(*this)(i,eReservations) = distribution(generator);
		(*this)(i,eBuyers) = disb_buyers(generator);
		(*this)(i, eBids) = disb_buyers(generator);
	}
}

	//TEST
