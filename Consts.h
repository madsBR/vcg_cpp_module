#pragma once
#include <cstdint>
#include <array>
#include <type_traits>
constexpr size_t cMax_item_nr{ 32 };
using int_t = uint_fast32_t;
std::array<int_t, cMax_item_nr> constexpr initialize_masks() {
	std::array<int_t, cMax_item_nr> result{};
	for (int i = 0; i < cMax_item_nr; i++) {
		result[i] = 1 << i;
	}
	return result;
};