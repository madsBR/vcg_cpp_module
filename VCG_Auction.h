#pragma once
#include <iostream>
#include <array>
#include <vector>
#include <type_traits>
#include "consts.h"
#include "Picks_And_Reservation_Matrix.h"
#include "Max_Bid_Computer.h"
#include "homebrew/ThreadPool/ThreadPool.h"
#include "homebrew/ThreadPool/Conc_Jobs_Container.h"
#include <algorithm>
#include <atomic>
#include <vector>
#include "Consts.h"
#include "Max_Bid_Runnable.h"
class VCG_Auction
{
	int bid_nr;
	int buyers_nr;
	int main_split = 3;
	int th_nr{ 2 };
	Conc_Jobs_Container jobs{};
	ThreadPool pool{jobs, 2 };
	std::vector<int> max_vals;
	std::vector<int> sub_max_vals;
	Array2D<int> winners;
	using out_iter = std::vector<int>::iterator;
	using out_iter_winner = Array2D<int>::iterator2;
	std::atomic_int32_t max_value{ 0 };
	Picks_And_Reservation_Matrix mat;


public:
	std::vector<Picks_And_Reservation_Matrix> submatrices;
	VCG_Auction(int bid_nr_, int buyers_nr_, Picks_And_Reservation_Matrix& bid_matrix)
		: bid_nr{ bid_nr_ }, buyers_nr{ buyers_nr_ }, mat{ bid_matrix }, winners{ main_split,buyers_nr_ },
		max_vals( main_split,0 ), sub_max_vals( buyers_nr_,0 )
	{
		submatrices = mat.create_submatrices();
	}	
	VCG_Auction(int bid_nr_, int buyers_nr_, int* bid_matrix) : bid_nr{ bid_nr_ },
		buyers_nr{ buyers_nr_ }, mat(bid_nr), winners{ main_split,buyers_nr_ }, max_vals(main_split,0 ), sub_max_vals( buyers_nr_,0 ) 
	{
		std::copy_n(bid_matrix, 4 * bid_nr_, mat.begin());
		submatrices = mat.create_submatrices();
	}
	// probably throw out constexpr static std::array<int_t, cMax_item_nr> masks{ initialize_masks() };

	void set_jobs() {
		// main jobs:
		int from{ 0 };
		int to{ 0 };
		auto iter = max_vals.begin();
		auto iter_winner = winners.begin1();
		int remaining = bid_nr;
		for(int i = 0;i<main_split-1;i++){
			from = to;
			to = i +1 == main_split ? remaining : (i+1)* (bid_nr / main_split);
			remaining -= bid_nr / main_split;
			auto iter_winner2 = winners.begin2(iter_winner);
			Max_Bid_Runnable_W_Winners<out_iter,out_iter_winner>* run_ptr = new  Max_Bid_Runnable_W_Winners<out_iter, out_iter_winner>(buyers_nr, mat, iter,iter_winner2,from,to);
			jobs.add_runnable(*run_ptr);
			iter++;
			iter_winner++;
		}
		
		jobs.add_latch(th_nr=2);
		iter = sub_max_vals.begin();
		for (int i = 0; i < buyers_nr; i++) {
			Max_Bid_Runnable<out_iter>* run_ptr = new  Max_Bid_Runnable<out_iter>(buyers_nr, submatrices[i], iter);
			jobs.add_runnable(*run_ptr);
			iter++;
		}
	}
	void start() {
		pool.start();
		Test::print_test(max_vals[0], max_vals[1], max_vals[2]);

	}

};

